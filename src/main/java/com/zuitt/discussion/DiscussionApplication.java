package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {SpringApplication.run(DiscussionApplication.class, args);
	}
	// Get all user
	// localhost:8080/users
	@GetMapping("/users")
	public String getPosts(){
		return "All users retrieved.";
	}
	//	Creating a user
	//	localhost:8080/users
	@PostMapping("/users")
	public String createPost(){
		return "New user created.";
	}
	//	Get a specific user
	//	localhost:8080/users/1234
	@GetMapping("/users/{userid}")
	public String getPost(@PathVariable Long userid){
		return "Viewing details of post " + userid;
	}

	//	Updating a post
	//	localhost:8080/users/1234
	//	Automatically converts the format to JSON.
	@PutMapping("/users/{userid}")
	@ResponseBody
	public Post updateUser(@PathVariable Long userid, @RequestBody Post name){
		return name;
	}

	//	localhost:8080/users/1234
	@DeleteMapping("/users/{userid}")
	public String deleteMyUser(@PathVariable Long userid, @RequestHeader(value = "Authorization") String user){
		if (!user.isEmpty()) {
			return "the user "+userid+" has been deleted";
		} else {
			return "Unauthorized access.";
		}
	}



}
